package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

const (
	ProductNameHeader      = "Product Name"
	ProductQtyHeader       = "Available Qty"
	TableCornerRune        = '+'
	TableDoubleHLineRune   = '='
	TableSingleHLineRune   = '-'
	TableSingleVLineRune   = '|'
	TableWhitespacePadding = 2
)

var (
	firstColLength, secondColLength int
)

/**
Check error and panic if not nil!
*/
func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	var inputFilePath string
	if len(os.Args) == 1 {
		inputFilePath = "C:/dev/trees/stockman/resources/input.csv"
	} else {
		inputFilePath = os.Args[1]
	}

	file, err := os.Open(inputFilePath)
	check(err)

	fileReader := bufio.NewReader(file)
	fileScanner := bufio.NewScanner(fileReader)
	fileScanner.Scan()
	headerLine := fileScanner.Text()
	if headerLine != "name,qty" {
		fmt.Println("WARN: Header line in file not as expected!")
	}

	stockList := []Stock{}
	for fileScanner.Scan() {
		line := fileScanner.Text()
		if !strings.HasPrefix(line, "#") {
			dataLine := strings.Split(line, ",")
			trimmedProductName := strings.Trim(dataLine[0], " ")
			trimmedProductQty, err := strconv.Atoi(strings.Trim(dataLine[1], " "))
			check(err)
			stockList = append(stockList, Stock{trimmedProductName, trimmedProductQty})
		} else {
			fmt.Println("Comment line found: " + line)
		}
	}

	stockQtyDescSorter := func(s1, s2 *Stock) bool {
		return s1.stockQty > s2.stockQty
	}
	By(stockQtyDescSorter).Sort(stockList)

	printTable(stockList)
}

func printTable(stockList []Stock) {
	setTableColumnLengths(stockList)

	var buffer bytes.Buffer
	buffer = prepareDoubleLine(buffer)
	buffer = prepareHeader(buffer)
	buffer = prepareDoubleLine(buffer)
	buffer = printStock(buffer, stockList)
	buffer = prepareSingleLine(buffer)
	fmt.Println(buffer.String())
}

func printStock(buffer bytes.Buffer, stockList []Stock) bytes.Buffer {
	for _, stock := range stockList {
		buffer = prepareDataEntry(buffer, stock)
	}
	return buffer
}

func prepareDataEntry(buffer bytes.Buffer, stock Stock) bytes.Buffer {
	buffer.WriteString(fmt.Sprintf("%c %s", TableSingleVLineRune, stock.productName))
	extraSpace := firstColLength - len(stock.productName)
	for i := 0; i < extraSpace-2; i++ {
		buffer.WriteString(" ")
	}
	buffer.WriteString(fmt.Sprintf("%c %d", TableSingleVLineRune, stock.stockQty))
	extraSpace = secondColLength - len(fmt.Sprintf("%d", stock.stockQty))
	for i := 0; i < extraSpace-2; i++ {
		buffer.WriteString(" ")
	}
	buffer.WriteString(fmt.Sprintf("%c\n", TableSingleVLineRune))
	return buffer
}

func prepareHeader(buffer bytes.Buffer) bytes.Buffer {
	buffer.WriteString(fmt.Sprintf("%c %s", TableSingleVLineRune, ProductNameHeader))
	extraSpace := firstColLength - len(ProductNameHeader)
	for i := 0; i < extraSpace-2; i++ {
		buffer.WriteRune(' ')
	}
	buffer.WriteString(fmt.Sprintf("%c %s", TableSingleVLineRune, ProductQtyHeader))
	extraSpace = secondColLength - len(ProductQtyHeader)
	for i := 0; i < extraSpace-2; i++ {
		buffer.WriteRune(' ')
	}
	buffer.WriteString(fmt.Sprintf("%c\n", TableSingleVLineRune))
	return buffer
}

func prepareDoubleLine(buffer bytes.Buffer) bytes.Buffer {
	return prepareLine(buffer, TableDoubleHLineRune)
}

func prepareSingleLine(buffer bytes.Buffer) bytes.Buffer {
	return prepareLine(buffer, TableSingleHLineRune)
}

func prepareLine(buffer bytes.Buffer, lineRune rune) bytes.Buffer {
	buffer.WriteRune(TableCornerRune)
	for i := 0; i < firstColLength-1; i++ {
		buffer.WriteRune(lineRune)
	}
	buffer.WriteRune(TableCornerRune)
	for i := 0; i < secondColLength-1; i++ {
		buffer.WriteRune(lineRune)
	}
	buffer.WriteString(fmt.Sprintf("%c\n", TableCornerRune))
	return buffer
}

func setTableColumnLengths(stockList []Stock) {
	firstColLength = len(ProductNameHeader)
	secondColLength = len(ProductQtyHeader)
	for _, stock := range stockList {
		productNameLength := len(stock.productName)
		if productNameLength > firstColLength {
			firstColLength = productNameLength
		}
		productQtyLength := len(fmt.Sprintf("%d", stock.stockQty))
		if productQtyLength > secondColLength {
			secondColLength = productQtyLength
		}
	}
	firstColLength += TableWhitespacePadding * 2
	secondColLength += TableWhitespacePadding * 2
}

type Stock struct {
	productName string
	stockQty    int
}

/**
StockSorter Implementation
*/

type By func(s1, s2 *Stock) bool

func (by By) Sort(stockList []Stock) {
	ps := &StockSorter{
		stockList: stockList,
		by:        by,
	}
	sort.Sort(ps)
}

func (s *StockSorter) Len() int {
	return len(s.stockList)
}

func (s *StockSorter) Swap(i, j int) {
	s.stockList[i], s.stockList[j] = s.stockList[j], s.stockList[i]
}

func (s *StockSorter) Less(i, j int) bool {
	return s.by(&s.stockList[i], &s.stockList[j])
}

type StockSorter struct {
	stockList []Stock
	by        func(s1, s2 *Stock) bool
}
