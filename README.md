# Stockman

Stockman - the project which starts from very humble beginnings and gradually evolves into a glorious Sales, Procurement and Inventory management software.

## Usage Instructions

Download `stockman.exe` and drag a CSV file detailing products and quantities on it to see it printed in table format and ordered by quantities in descending order.

## Sample CSV File

```
name,qty
# nunc dimittis
p1,    100
# lorem ipsum dolor
p2  ,23
p3,    90
# sit amet
```
